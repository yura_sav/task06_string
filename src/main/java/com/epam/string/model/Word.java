package com.epam.string.model;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Word {
    private String word;

    Word(String word) {
        this.word = word;
    }

    public int getLength() {
        return word.length();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Word)) return false;
        Word word1 = (Word) o;
        return word.equals(word1.word);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word);
    }

    @Override
    public String toString() {
        return word;
    }

    public boolean isStartWithVowelLetter() {
        Pattern pattern = Pattern.compile("[aeiou].*", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(word);
        return matcher.matches();
    }

}

package com.epam.string.view;

@FunctionalInterface
public interface Printable {
    void print();
}

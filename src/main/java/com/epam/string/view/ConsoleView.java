package com.epam.string.view;

import com.epam.string.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.*;

public class ConsoleView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner scanner = new Scanner(System.in);
    private Locale locale;
    private ResourceBundle bundle;

    private static Logger logger = LogManager.getLogger(ConsoleView.class);

    public ConsoleView() {
        controller = new Controller();
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::internationalizeMenuUkrainian);
        methodsMenu.put("2", this::internationalizeMenuEnglish);
        methodsMenu.put("3", this::internationalizeMenuRussian);
        methodsMenu.put("4", this::printNumberOfRepeatedWords);
        methodsMenu.put("5", this::printSortedSentencesByLengthOfWords);
        methodsMenu.put("6", this::printWordsThatAreOnlyInFirstSentence);
        methodsMenu.put("7", this::printWordsFixedLengthInQuestions);
        methodsMenu.put("8", this::printChangedFirstVowelLetterOnLongestWord);
    }

    private void setMenu() {

        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
        menu.put("9", bundle.getString("9"));
    }

    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuRussian() {
        locale = new Locale("ru");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }


    private void showMenu() {
        System.out.println();
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    private void printNumberOfRepeatedWords() {
        logger.info(controller.countNumberOfRepeatedWords());
    }

    private void printSortedSentencesByLengthOfWords() {
        logger.info(controller.sortListByLength());
    }

    private void printWordsThatAreOnlyInFirstSentence() {
        logger.info(controller.findWordsInFirstSentence());
    }

    private void printWordsFixedLengthInQuestions() {
        logger.info("enter length of word: ");
        try {
            logger.info(controller.findUniqueWords(scanner.nextInt()));
        } catch (IOException e) {
            logger.fatal(e.getMessage());
        }
    }

    private void printChangedFirstVowelLetterOnLongestWord() {
        logger.info(controller.changeFirstVowelLetterOnTheLongestWord());
    }

    public void show() {
        String keyMenu;
        do {
            showMenu();
            logger.info("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            if (keyMenu.equals("Q")) {
                logger.info("bye-bye");
                System.exit(0);
            }
            try {
                methodsMenu.get(keyMenu).print();
            } catch (NullPointerException e) {
                logger.error("incorrect inputs");
                break;
            }
        } while (true);
    }
}

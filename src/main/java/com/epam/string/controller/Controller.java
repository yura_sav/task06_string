package com.epam.string.controller;

import com.epam.string.model.ReadingFromFile;
import com.epam.string.model.Sentence;
import com.epam.string.model.Text;
import com.epam.string.model.Word;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class Controller {
    private static final String fileName = "C:\\firstCourse\\task06_string\\src\\main\\resources\\Text.txt";
    private Text text;

    public Controller() {
        try {
            text = new Text(ReadingFromFile.readFileAsString(fileName));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    //map with sentence and number of repeated words
    public Map<Sentence, Long> countNumberOfRepeatedWords() {
        Map<Sentence, Long> map = new HashMap<>();
        for (Sentence sentence : text.getText()) {
            Map<Word, Long> wordCounting = sentence.getSentence().stream().collect(groupingBy((x -> x), counting()));
            long sumOfRepeatedWords = 0;
            for (Word word : wordCounting.keySet()) {
                if (wordCounting.get(word) != 1) {
                    sumOfRepeatedWords += wordCounting.get(word);
                }
            }
            map.put(sentence, sumOfRepeatedWords);
        }
        return map;
    }

    //compare sentence by length of sentences
    public List<Sentence> sortListByLength() {
        Comparator<Sentence> comparator = Comparator.comparingInt(o -> o.getSentence().size());
        List<Sentence> list = new LinkedList<>(text.getText());
        list.sort(comparator);
        return list;
    }

    //list of words that are only in first sentence
    public List<Word> findWordsInFirstSentence() {
        boolean b;
        Sentence sentence = text.getText().get(0);
        List<Sentence> listOfSentence = text.getText();
        List<Word> listOfUniqueWords = new LinkedList<>();
        for (Word word : sentence.getSentence()) {
            b = true;
            for (int i = 1; i < listOfSentence.size(); i++) {
                long countOfNotUniqueWords = listOfSentence.get(i).getSentence().stream().filter(word::equals).count();
                if (countOfNotUniqueWords > 0)
                    b = false;
            }
            if (b) {
                listOfUniqueWords.add(word);
            }
        }
        return listOfUniqueWords;
    }

    public Set<Word> findUniqueWords(int length) throws IOException {
        text.getText().get(0).getQuestionSentences(ReadingFromFile.readFileAsString(fileName));
        Set<Word> list = new LinkedHashSet<>();
        for (Sentence sentence : text.getText()) {
            if (sentence.toString().contains("?")) {
                list.addAll(sentence.getSentence());
            }
        }
        return list.stream().filter(word -> word.getLength() == length).collect(Collectors.toSet());
    }

    public Text changeFirstVowelLetterOnTheLongestWord() {
        int indexOfLargestWord = 0;
        Text changedText = text;
        for (Sentence sentence : changedText.getText()) {
            long theLargestWord = sentence.getSentence().stream().mapToInt(Word::getLength).max().getAsInt();
            if (sentence.getSentence().get(0).isStartWithVowelLetter()) {
                indexOfLargestWord = 0;
                for (Word word : sentence.getSentence()) {
                    if (word.getLength() == theLargestWord) {
                        Collections.swap(sentence.getSentence(), 0, indexOfLargestWord);
                    }
                    indexOfLargestWord++;
                }
            }
        }
        return changedText;

    }

}
